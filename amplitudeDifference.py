import alsaaudio
import numpy
import matplotlib.pyplot as plt
import wave
from sys import byteorder
from array import array
from struct import pack
import requests
import json_tricks as json
import threading
NUMBEROFFRAMES = 500
FRAMESIZE = 100
SAMPLINGRATE = 44100
FORMAT = alsaaudio.PCM_FORMAT_S16_LE
inp1 = alsaaudio.PCM(alsaaudio.PCM_CAPTURE,alsaaudio.PCM_NONBLOCK,'default',1)
inp1.setchannels(1)
inp1.setrate(SAMPLINGRATE)
inp1.setformat(FORMAT)
inp1.setperiodsize(FRAMESIZE)
inp2 = alsaaudio.PCM(alsaaudio.PCM_CAPTURE,alsaaudio.PCM_NONBLOCK,'default',2)
inp2.setchannels(1)
inp2.setrate(SAMPLINGRATE)
inp2.setformat(FORMAT)
inp2.setperiodsize(FRAMESIZE)
inp3 = alsaaudio.PCM(alsaaudio.PCM_CAPTURE,alsaaudio.PCM_NONBLOCK,'default',3)
inp3.setchannels(1)
inp3.setrate(SAMPLINGRATE)
inp3.setformat(FORMAT)
inp3.setperiodsize(FRAMESIZE)

url = "https://sound-radar.firebaseio.com/raw-data.json"
headers = {'content-type':'application/json', 'Accept-Charset':'UTF-8'}
print("Recording")

def pushToFB(jsondata):
    print("Pushing to firebase")
    r = requests.post(url,data=jsondata,headers=headers)
    print(r)
    print("Done pushing")
    return None
while 1:
    amp1 = []
    amp2 = []
    amp3 = []
    while len(amp1) < 5000:
        l1, data1 = inp1.read()
        l2, data2 = inp2.read()
        l3, data3 = inp3.read()
        if l1>0 and l2>0 and l3>0 and l1==l2 and l1==l3:
            a = numpy.fromstring(data1, dtype='int16')
            amp1.extend(a)
            b = numpy.fromstring(data2, dtype='int16')
            amp2.extend(b)
            c = numpy.fromstring(data3, dtype='int16')
            amp3.extend(c)
    jsons = {}
    jsons['mic1'] = str(amp1)
    jsons['mic2'] = str(amp2)
    jsons['mic3'] = str(amp3)
    print("Dumping to Json")
    data = [json.dumps(jsons)]
    thr = threading.Thread(target=pushToFB, args=(data),kwargs={})
    thr.start()

