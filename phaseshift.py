import pyaudio
import struct
import matplotlib.pyplot as plt
import numpy
#import time

FORMAT = pyaudio.paFloat32
SAMPLEFREQ = 44100
FRAMESIZE = 1024
NOFFRAMES = 10
p = pyaudio.PyAudio()
print('running')
stream1_data = []
stream2_data = []
def stream1_callback(in_data, frame_count, time_info, status):
    global stream1_data
    decoded = struct.unpack(str(frame_count)+'f',in_data)
    stream1_data.extend(decoded)
    #plt.plot(decoded, c = 'r')
    return (in_data,pyaudio.paContinue)
def stream2_callback(in_data, frame_count, time_info, status):
    global stream2_data
    decoded = struct.unpack(str(frame_count)+'f',in_data)
    stream2_data.extend(decoded)
    #plt.plot(decoded, c = 'b')
    return (in_data,pyaudio.paContinue)

#for i in range(p.get_device_count()):
    #dev = p.get_device_info_by_index(i)
    #print((i, dev['name'],dev))

stream1 = p.open(format=FORMAT,channels=1,rate=SAMPLEFREQ,input=True, frames_per_buffer=FRAMESIZE,input_device_index =2, stream_callback = stream1_callback)
stream2 = p.open(format=FORMAT,channels=1,rate=SAMPLEFREQ,input=True,frames_per_buffer=FRAMESIZE,input_device_index = 3, stream_callback = stream2_callback)

print("starting steams")
stream1.start_stream()

stream2.start_stream()


plt.ion()
while 1:
    #data1 = stream1.read(NOFFRAMES*FRAMESIZE, exception_on_overflow = False)
    #data2 = stream2.read(NOFFRAMES*FRAMESIZE, exception_on_overflow = False)
    #decoded1 = struct.unpack(str(NOFFRAMES*FRAMESIZE)+'f',data1)
    if len(stream1_data) > 0:
        plt.plot(stream1_data, c = 'r')
        stream1_data = []
    #decoded2 = struct.unpack(str(NOFFRAMES*FRAMESIZE)+'f',data2)
    if len(stream2_data) > 0:
        plt.plot(stream2_data, c = 'b')
        stream2_data = []
    try:
        plt.pause(0.05)
    except:
        stream1.stop_stream()
        stream2.stop_stream()
        stream1.close()
        stream2.close()
        p.terminate()
        quit()
    #time.sleep(0.05)

#data = stream.read(NOFFRAMES*FRAMESIZE)
#decoded = struct.unpack(str(NOFFRAMES*FRAMESIZE)+'f',data)

#stream.stop_stream()
#stream.close()
#p.terminate()
#print('done ')
#plt.plot(decoded)
#plt.show()
#x = numpy.fft(decoded)
#plt.plot(x)
#plt.show()
